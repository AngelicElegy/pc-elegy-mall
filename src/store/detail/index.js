// detail详情页的仓库
import { reqGoodsList } from "@/api"
import { postShopOrder } from "@/api";
// 封装游客的ID，用nanoid生成一个随机的ID,不能在变化了
import { getID } from "@/utils/nanoid_tokens"
const state = {
    GoodsList: {},
    nanoid_token:getID()
}
const actions = {
    async GetGoodsList({ commit }, skuid) {
        let result = await reqGoodsList(skuid);
        if (result.code == 200) {
            commit('GETGOODSLIST', result.data)
        }
    },
    async PostShopData({commit},{ skuid, skuNum }) {
        let result = await postShopOrder(skuid, skuNum)
        // 因为不需要获取数据来存储,后续的操作
        // 判断提交是否成功
        if(result.code==200)
        {
            return 'ok';
        }
        else{
            return Promise.reject(new Error('faile'));
        }
    }
}
const mutations = {
    GETGOODSLIST(state, GoodsList) {
        state.GoodsList = GoodsList
    }

}
// 简化数据
const getters = {
    // 后面再传个对象是为了兜底，防止前面的数据没就位，显示undefined
    categoryView(state) {
        return state.GoodsList.categoryView || {};
    },
    skuInfo(state) {
        return state.GoodsList.skuInfo || {};
    },
    spuSaleAttrList(state) {
        return state.GoodsList.spuSaleAttrList || [];
    }
}
export default {
    state,
    actions,
    mutations,
    getters
}