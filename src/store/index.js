import Vue from "vue";
import Vuex from "vuex"
Vue.use(Vuex)
import home from "./home";
import search from "./search";
import detail from "./detail";
import shoplist from "./shoplist";
import user from "./user";
import trade from "./trade"
export default new Vuex.Store({
//    vuex模块式开发
    modules:{
        home,
        search,
        detail,
        shoplist,
        user,
        trade
   }
})