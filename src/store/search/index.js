// search模块的仓库
import { reqSearchList } from "@/api"
const state = {
    SearchList: {}
}
const actions = {
    async GetSearchList({ commit }, params = {}) {
        let result = await reqSearchList(params);
        if (result.code == 200) {
            commit('GETSEARCHLIST', result.data)
        }
    }
}
const mutations = {
    GETSEARCHLIST(state, SearchList) {
        state.SearchList = SearchList
    }
}
const getters = {
    attrsList(state){
        return state.SearchList.attrsList||[]
    },
    goodsList(state){
        return state.SearchList.goodsList||[]
    },
    trademarkList(state){
        return state.SearchList.trademarkList||[]
    }
}
export default {
    state,
    actions,
    mutations,
    getters
}