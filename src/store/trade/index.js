import { reqAddressInfo,reqOrderInfo} from "@/api"
const state={
    addressInfo:[],
    orderInfo:{}
}
const actions={
    async getAddressInfo({commit}){
        let result=await reqAddressInfo()
        if(result.code==200){
            commit('GETADDRESSINFO',result.data)
        }
    },
    async getOrderInfo({commit}){
        let result=await reqOrderInfo()
        if(result.code==200){
            commit('GETORDERINFO',result.data)
        }
    }

}
const mutations={
    GETADDRESSINFO(state,addressInfo){
        state.addressInfo=addressInfo;
    },
    GETORDERINFO(state,orderInfo){
        state.orderInfo=orderInfo;
    }
}
const getters={}
export default {
    state,
    actions,
    mutations,
    getters
}