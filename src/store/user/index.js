import { reqPhoneNode } from "@/api"
import { toRegister } from "@/api"
import { toLogin } from "@/api"
import { reqAuthName } from "@/api"
import { toLogout } from "@/api"
import { getToken,setToken,removeToken} from "@/utils/token"
let state={
    phoneNode:'',
    token:getToken(),
    authName:{}
}
let actions={
    // 获取验证码
    async GetPhoneNode({commit},phone){
        let result=await reqPhoneNode(phone)
        if(result.code==200){
            commit('GETPHONENODE',result.data)
            return '发送成功'
        }
        else {
            return Promise.reject(new Error('faile'))
        }
    },
    // 注册
    async GoRegister({commit},user){
        let result=await toRegister(user);
        if(result.code==200)
        {
            return 'OK'
        }else{
           return Promise.reject(new Error('faile') )
        } 
    },
    // 登陆
    async GoLogin({commit},userMsg){
        let result= await toLogin(userMsg);
        if(result.code==200)
        {
            commit('GOLOGIN',result.data.token)
            setToken(result.data.token)
            return 'OK'
        }
        else{
            return Promise.reject(new Error('faile'))
        }
    },
    // 获取用户名展示在上方
    async GetAuthName({commit}){
        let result=await reqAuthName()
        if(result.code==200)
        {
            commit('GETAUTHNAME',result.data)
            return 'OK'
        }
        else{
            return Promise.reject(new Error('faile'))
        }
    },
    // 退出登录
    async GoLogout({commit}){
        let result=await toLogout();
        if(result.code==200){
        // 如果状态码=200，说明后端服务器清除成功
        // 那么我们就需要再清除前端用户留下来的信息即可
        // 但是actions里面不能操作state，故转到mutations
        commit('CLEAR')
        return 'OK'
        }else{
            return Promise.reject(new Error('faile'))
        }
    }
}
let mutations={
    GETPHONENODE(state,phoneNode){
        state.phoneNode=phoneNode;
    },
    GOLOGIN(state,token){
        state.token=token;
    },
    GETAUTHNAME(state,authName){
        state.authName=authName;
    },
    CLEAR(state){
        state.token='';
        state.authName={};
        removeToken();
    }

}
let getters={

}

export default {
    state,
    actions,
    mutations,
    getters
}