// home模块的仓库
import { reqCategoryList,reqGetBannerList,reqGetFloorList } from '@/api'
const state = {
    CategoryList:[],
    BannerList:[],
    FloorList:[]
}
const actions = {
    async CategoryList({commit}) {
        let result=await reqCategoryList();
        if(result.code==200){
            commit("CATEGORYLIST",result.data)
        }
    },
    async BannerList({commit}) {
        let result=await reqGetBannerList();
        if(result.code==200){
            commit("BANNERLIST",result.data)
        }
    },
    async FloorList({commit}) {
        let result=await reqGetFloorList();
        if(result.code==200){
            commit("FLOORLIST",result.data)
        }
    }
}
const mutations = {
    CATEGORYLIST(state,CategoryList){
        state.CategoryList=CategoryList
    },
    BANNERLIST(state,BannerList){
        state.BannerList=BannerList
    },
    FLOORLIST(state,FloorList){
        state.FloorList=FloorList
    }
}
const getters = {}
export default {
    state,
    actions,
    mutations,
    getters
}