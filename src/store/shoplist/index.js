import { reqShopList } from '@/api/index'
import { deleteGood } from '@/api/index';
import { cartChecked } from '@/api/index'
const state = {
    ShopList:[]
};
const actions = {
    async getShopList({ commit }) {
        let result = await reqShopList()
        if(result.code==200)
        {
            commit('GETSHOPLIST',result.data)
        }
    },
    async deleteGoodList({commit},skuId){
        let result=await deleteGood(skuId)
        if(result.code==200)
        {
            return 'OK'
        }
        else {
            return Promise.reject(new Error('faile'));
        }
    },
    deleteChecked({dispatch,getters}){
        // 从context上下文中解构出来对应需要的东西dispatch和getters
        // 获取购物车中全部的产品
        let promiseAll=[];
        getters.ShopList.cartInfoList.forEach(item => {
        // 判断如果该item项的ischecked=1才调用删除的按钮
        // 调用本仓库中之前写过的单个删除按钮
        let promise=item.isChecked==1?dispatch("deleteGoodList",item.skuId):'';
       
    // 因为这个函数调用了会返回一个Promise对象，所以我们将其接收下来存放在数组中
        promiseAll.push(promise)        
        // 最后返回对应的成功或者失败的结果
        // Promise.all([p1,p2,p3..]),其中的p1,p2,p3都是promise，如果有一个失败，那就都失败，否则都成功
        // 最后这个return的目的就是确保每一个被勾选的内容都被成功的删除掉
        // 如果有一个地方是失败的,那么总体的就是失败的
        return Promise.all(promiseAll)
        });
    },
    // 修改购物车某个产品的选中状态
    async setCartChecked({commit},{skuId,isChecked})
    {
        let result=await cartChecked(skuId,isChecked)
        if(result.code==200)
        {
            return 'OK'
        }
        else {
            return Promise.reject(new Error('faile'))
        }
    },
    updateAllIsChecked({dispatch,state},isChecked){
        let promiseAll=[];
        state.ShopList[0].cartInfoList.forEach(item => {
        let promise = dispatch('setCartChecked',{skuId:item.skuId,isChecked})
        promiseAll.push(promise)
    });
    return Promise.all(promiseAll)
    }
};
const mutations = {
    GETSHOPLIST(state,ShopList){
        state.ShopList=ShopList;
    }
};
const getters = {
    ShopList(state){
        return state.ShopList[0]||{}
    }
};
export default {
    state,
    mutations,
    actions,
    getters
}