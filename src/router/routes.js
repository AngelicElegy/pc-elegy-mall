// 引入路由组件
import Home from '@/pages/Home'
import Login from '@/pages/Login'
import Register from '@/pages/Register'
import Search from '@/pages/Search'
import Detail from '@/pages/Detail'
import AddCartSuccess from '@/pages/AddCartSuccess'
import Trade from '@/pages/Trade'
import Pay from '@/pages/Pay'
import Center from '@/pages/Center'
import myOrder from '@/pages/Center/myOrder'
import groupOrder from '@/pages/Center/groupOrder'
import PaySuccess from '@/pages/PaySuccess'
import ShopCart from '@/pages/ShopCart'
export default
    [ {
        path:"/home",
        component:Home,
        // show是显示footer组件的
        meta:{
            show:true
        }
    },
    {
        path:"/login",
        component:Login,
        meta:{
            show:false
        }
    },
    {
        path:"/trade",
        component:Trade,
        meta:{
            show:true
        }
    },
    {
        path:"/center",
        component:Center,
        meta:{
            show:true
        },
        children:[{
            path:'myorder',
            component:myOrder
        },{
            path:'grouporder',
            component:groupOrder
        },{
            // 路由重定向
            // 让页面刚进来的时候不显示空白，而是我的订单页面
            path:'/center',
            redirect:'/center/myorder'
        }]
    },
    {
        path:"/pay",
        component:Pay,
        meta:{
            show:true
        }
    },
    {
        path:"/paysuccess",
        component:PaySuccess,
        meta:{
            show:true
        }
    },
    {
        path:"/register",
        component:Register,
        meta:{
            show:false
        }
    },  
    {
        name:'detail',
        path:"/detail/:skuid",
        component:Detail,
        meta:{
            show:true
        }
    },
    {
        name:'addcartsuccess',
        path:"/addcartsuccess",
        component:AddCartSuccess,
        meta:{
            show:true
        }
    },
    {
        name:'shopcart',
        path:"/shopcart",
        component:ShopCart,
        meta:{
            show:true
        }
    },
    {
        name:'search',
        path:"/search/:keyword?",
        component:Search,
        meta:{
            show:true
        }
    },
    // 重定向，在项目初始化访问/时，就自动跳转到重定向的位置
    {
        path:"*", //*就是根的意思
        redirect:"/home"
    }]