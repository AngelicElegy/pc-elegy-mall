import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter)
import routes from "./routes";
import store from "@/store";
// 重写push和replace
let OriPush=VueRouter.prototype.push
let OriReplace=VueRouter.prototype.replace
VueRouter.prototype.push=function(location,resolve,reject){
    if(resolve && reject){
        OriPush.call(this,location,resolve,reject)
    }
    else {
        OriPush.call(this,location,()=>{},()=>{})
    }
}
VueRouter.prototype.replace=function(location,resolve,reject){
    if(resolve && reject){
        OriReplace.call(this,location,resolve,reject)
    }
    else {
        OriReplace.call(this,location,()=>{},()=>{})
    }
}
// 配置路由
let router = new VueRouter({
    routes,
    // 滚动行为
    scrollBehavior(to,from,savedPosition){
        return {
            // 滚动条到最上方
           y:0
        }
    }
})
// 配置(全局)前置路由守卫
router.beforeEach(async (to,from,next)=>{
    // 通过token来判断用户是否登录
    let token=store.state.user.token;
    // 获取用户名
    let userName=store.state.user.authName.name;
    // 已登录
    if(token){
        if(to.path=='/login'||to.path=='/register')
        {
            next('/home')
        }else{
            // 判断左上角有没有用户名,有就通过
            if(userName)
            {
               next() 
            }
            // 没有的话，就再派发一下获取
            else{
                try {
                    // 获取用户名称展示在左上角
                    await store.dispatch('GetAuthName');
                    next()  
                } catch (error) {
                    // 如果失败了，那就是token失效了
                    // 所以要重新登录，获取token
                    await store.dispatch('GoLogout');
                    next('/login')
                 }

            }
        }
    }
    // 未登录
    else{
        if(to.path=='/shopcart'||to.path=='/addcartsuccess')
        {
            next('/login')
        }
        else{
            next()
        }
    }
})
export default router;