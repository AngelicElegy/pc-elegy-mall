// 二次封装axios
import store from "@/store";
import axios from "axios";
import { Promise } from "core-js";
import nProgress from "nprogress";
// 引入进度条的样式
import "nprogress/nprogress.css";
// 目的:封装请求和响应拦截器
// 1.利用axios对象的create方法，创建一个axios实例
const requests = axios.create({
    // request其实就是axios

    // 基础路径:发请求时，路径中会默认出现api，不用自己加
    baseURL: '/api',
    // 请求超时的时间
    timeout: 5000
})
// 请求拦截器 :发请求前可以做一些事情
requests.interceptors.request.use((config) => {
    // 配置请求头nanoid来标识游客的身份
    if (store.state.detail.nanoid_token) {
        config.headers.userTempId = store.state.detail.nanoid_token;
    }
    // 判断需要携带Token给服务器
    if (store.state.user.token) {
        config.headers.token = store.state.user.token;
    }
    // 配置开始进度条
    nProgress.start()
    // config:配置对象  里面有header请求头
    return config
})
// 响应拦截器 :收响应后可以做一些事情
requests.interceptors.response.use(
    (res) => {
        // 配置结束进度条
        nProgress.done()
        // 服务器响应成功的回调函数
        return res.data
    }, (error) => {
        // 服务器响应失败的回调函数
        return Promise.reject(new Error('faile'))
    })

export default requests;