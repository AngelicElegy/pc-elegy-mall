// 对项目的所有接口进行统一的管理product
import requests from "./ajax";
import mockRequset from "./mockAjax"
// 三级联动接口
// /api/product/getBaseCategoryList get 无参
export const reqCategoryList=()=>requests({
        url:'/product/getBaseCategoryList',
        method:'get'
    })
// 获取首页轮播图的接口
export const reqGetBannerList=()=>mockRequset({
        url:'/banner',
        method:'get'
})
// 获取家用电器轮播图的接口
export const reqGetFloorList=()=>mockRequset({
        url:'/floor',
        method:'get'
})
// 获取搜索页面数据的接口
export const reqSearchList=(params)=>requests({
        url:'/list',
        method:'post',
        data:params
})
// 获取detail详情页数据的接口
export const reqGoodsList=(skuid)=>requests({
        url:`/item/${skuid}`,
        method:'get'
})
// 提交购物车的接口
export const postShopOrder=(skuid,skuNum)=>requests({
        url:`/cart/addToCart/${skuid}/${skuNum}`,
        method:'post'
})
export const reqShopList=()=>requests({
        url:`/cart/cartList`,
        method:'get'
})
export const deleteGood=(skuId)=>requests({
        url:`/cart/deleteCart/${skuId}`,
        method:'delete'
})
export const cartChecked=(skuId,isChecked)=>requests({
        url:`/cart/checkCart/${skuId}/${isChecked}`,
        method:'get'
})
export const reqPhoneNode=(phone)=>requests({
        url:`/user/passport/sendCode/${phone}`,
        method:'get'
})
export const toRegister=(data)=>requests({
        url:`/user/passport/register`,
        method:'post',
        data
})
export const toLogin=(data)=>requests({
        url:`/user/passport/login`,
        method:'post',
        data
})
export const reqAuthName=()=>requests({
        url:`/user/passport/auth/getUserInfo`,
        method:'get'
})
export const toLogout=()=>requests({
        url:`/user/passport/logout`,
        method:'get'
})
export const reqAddressInfo=()=>requests({
        url:`/user/userAddress/auth/findUserAddressList`,
        method:'get'
})
export const reqOrderInfo=()=>requests({
        url:`/order/auth/trade`,
        method:'get'
})
export const reqSubmitOrder=(tradeNo,data)=>requests({
        url:`/order/auth/submitOrder?tradeNo=${tradeNo}`,
        method:'post',
        data
})
export const reqUserOrder=(orderId)=>requests({
        url:`/payment/weixin/createNative/${orderId}`,
        method:'get'
})
export const reqOrderState=(orderId)=>requests({
        url:`/payment/weixin/queryPayStatus/${orderId}`,
        method:'get'
})