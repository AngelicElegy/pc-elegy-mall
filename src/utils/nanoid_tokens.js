import { nanoid } from "nanoid";
// 生成一个随机的字符串存储起来，每次执行都不变化
export const getID= ()=>{
    const nanoid_token=localStorage.getItem('nanoid');
    if(!nanoid_token)
    {
        nanoid_token=nanoid()
        localStorage.setItem('nanoid',nanoid_token)
    }
    return nanoid_token;
}
