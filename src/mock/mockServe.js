import Mock from "mockjs";
// JSON并没有暴露，为什么可以直接引入
// webpack默认对外暴露图片，JSON字符串
import banner from './banner.json'
import floor from './floor.json'
// 参数1:请求地址 参数2:请求数据
Mock.mock("/mock/banner",{code:200,data:banner});
Mock.mock("/mock/floor",{code:200,data:floor});
