import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/mock/mockServe'
import 'swiper/swiper-bundle.min.css'
// 引入三级联动组件
import TypeNav from '@/components/TypeNav'
import Pagination from '@/components/Pagination'
// 按需引入ElementUI组件库
import {MessageBox} from 'element-ui'
Vue.component(TypeNav.name, TypeNav)
Vue.component(Pagination.name,Pagination )
// 将elementUI的弹出层注册到Vue的原型上
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.config.productionTip = false
// 新知识点:统一引入
import * as API from '@/api'
new Vue({
  render: h => h(App),
  beforeCreate() {
    Vue.prototype.$bus = this;
    // 将统一引入的接口放在原型对象上
    Vue.prototype.$api = API;
  },
  router,
  store
}).$mount('#app')
